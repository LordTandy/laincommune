* What do you hope to accomplish by living in a laincommune? (please try to answer in 'I' statements as much as possible)
* What values would you like to see the community embody/espouse? (please try to be concise, <10)
* What type of environment do you envision the commune to be located in? (urban/rural, continent, climate etc)
* What can you contribute, in terms of capital or labor, to starting up the commune?
* Once the commune is active, what can you contribute to the community on an ongoing basis?
* Do you have any restrictions that would effect the location or necessary commune services, disabilities, serious illness, allergies, drug addiction, etc?
* Do you have experience living in a commune? starting a commune?

## Texas Guy

    - I want a free and open space to collaborate with other interesting persons and eventually a relatively secure and conducive location to the production of my clonal offspring and Cryonic storage facility.

    -I want the community to be cooperative, but simultaneously supportive of individuality. I want us to be free of the economic, and governmental pressures that exist in most countries today. Essentially, by becoming an example of a successful union of egoists, I hope we demonstrate the obselecence of government as it exists today.

    -I envision the commune to exist someplace temperate or dessert, probably in a rural zone not more than 150 miles from a major metroplex.

    -I have a regular interest income, that is quite modest. I understand financial planning for long term operations in addition to being a applied physicist, mostly in robotics, by training. I can contribute my efforts in automating processes and designing systems and devices to improve quality of life, while simultaneously providing insight and direction for financial security of the union if we so desire.

    -As far as ongoing contributions, I can maintain vital subsystems and maake continuous efforts to automate dull dirty and dangerous labor to maximize the amount of free time and quality of life for every member of the union. I can also setup and maintain a cryonic storage facility and personal cloning service that could allow for backup, storage and restoration of lains in the medium to very long term future.  Further, I have 1kw of solar panels and 300 ah of lithium ion battery storage that could be used to power a variety of appliances or charge additional batteries for ruunning other systems. It is a small amount of power, but I already have it and it is securely mounted to my home with a AC inverter unit to plug things directly into. 

    -I have no scruples or personal weaknesses besides being skinny. But labor for survival will likely change that quickly.

    -I have started student organizations and maintained and lead them as their president, and treasurer. I have never started a "commune". or participated in what I would call one.
         
## Ev

    -What do you hope to accomplish by living in a laincommune? 

My long-term goal is to either join or start a communal living situation inclusive of my poly-amorous group in a rural setting where we can raise animals and establish sustaining permaculture ecosystems. In the short term, I am relocating to NYC and would like to associate with a communal living space with like minded people there, or work on starting a more rural space in my spare time. I'm excited to get experience living and working in a communal space. 

    -What values would you like to see the community embody/espouse? (please try to be concise, <10)

Burning Man values line up with my own pretty well. I'd like to associate with a community that values:
    Respect for the environment, minimizing waste, impact and encouraging flourishing ecosystems
    Inclusive community. All members of the community are important and have inherent, equal value. Everyone's voice should be heard.
    Communal Effort. Not everything needs to be done together, but opportunities for learning and participation should not be wasted.
    Participatory civics. Everyone has a duty to raise issues with the group, voice their opinions, and facilitate discussion.
    Share/Gift Culture. The community operates on sharing and gifting without conditions or barter. Everyone pitches in on community tasks and shares the output of community effort. Caveat: personal property and privacy.

    -What type of environment do you envision the commune to be located in? (urban/rural, continent, climate etc)

As I am relocating to NYC, I would be open to a commune site there, in an urban setting, or in an outlying area that I can contribute to on the weekends/spare time (initially). Eventually I will prefer a rural setting with some access to a major metro area, possibly up-state NY, New Jersey, Pennsylvania.

    -What can you contribute, in terms of capital or labor, to starting up the commune?

I can share some burden of startup cost in the form of a modest amount of capital. I do not anticipate seeking any kind of exchange for that capital, I would probably consider it a sunk cost instead of an investment. I will be able to put a number to how much when considering the terms of association. Depending on the location of the commune, I can contribute a certain amount of labor to establish habilitation, either in the form of un/semi-skilled labor or moving various processes forward. I have lived in various austere conditions in the past and have had wilderness survival training.

    -Once the commune is active, what can you contribute to the community on an ongoing basis?

I have various skills in computer science, data analysis, leadership/management and I can teach yoga. I also intend to learn and apply permaculture practices. I hope to contribute in setting up community management and conflict resolution systems that are fair and inclusive to the members.

    -Do you have any restrictions that would effect the location or necessary commune services, disabilities, serious illness, allergies, drug addiction, etc?

I am physically healthy, no drug history, no major allergies, and able bodied.
         -Do you have experience living in a commune? starting a commune?
No, and no. Ive been placed in various leadership/management roles in the past for mid-large organizations but I am opposed to heirarchy and prefer more of a "peer-leader" style. Ive also been looking into communal living mechanics for a few months and am excited about the prospect.


## muskox
    > What do you hope to accomplish by living in a laincommune?
    
    I want to be a part of a system which enables interesting people to work on interesting things. Specifically, I'd like to work on interesting things myself, and I'd like to collaborate with other like-minded people. This commune project is an example of one of the kinds of things that I find interesting.
    The reason that I want to be a part of a commune project instead of an urban makerspace or other less-involved orginization is that I feel that a large obstacle to a certain kind of people (including myself) from being productive is the necessity to provide for their physical needs before their more abstract needs. I want to explore productivity and creativity as a lifestyle, not just as a hobby, and I can't do that while spending 40+ hours a week doing something else. 
    
    > What values would you like to see the community embody/espouse?
    
    - Productivity. Creation of cool things, production of useful goods and services.
    - Liberty. Members should be able to choose what is valueable/important to them.
    - Democracy. Each member should have an equal share of the collective power.
    
    > What type of environment do you envision the commune to be located in?
    
    Rural. I'd only consider joining an urban commune after it was already established, and even then I'd be hesitant. I've lived in a semi-rural/suburban area for my whole life, and cities make me nervous.
    As for the continent, it would be easier for me to help start a commune in North America, as that's where I'm currently located. I'm used to a fairly warm climate but would be willing to relocate depending on land availability/circumstances.
    
    > What can you contribute, in terms of capital or labor, to starting up the commune?
    
    A few thousand dollars of capital. I'm willing to do any manual labour necessary although I don't have much experience in the things that would be most useful: construction and agriculture.
    
    > Once the commune is active, what can you contribute to the community on an ongoing basis?
    
    Physical labour, and whatever skills I'm able to learn.
    
    > Do you have any restrictions that would effect the location or necessary commune services, disabilities, serious illness, allergies, drug addiction, etc?
    
    No.
    
    > Do you have experience living in a commune? starting a commune?
    
    No.
    
An enumerated list of things I would like to have in an established commune (while poor living conditions might necessarily have to be tolerated during the intial period, I wouldn't want to go without any of these for a prolonged period of time):
    
    Physical Needs
    
    1. Clean, non-polluted air to breath.
    2. Clean drinking water and nutritional food, with adequate amounts of protein, carbohydrates, fats, and basic vitamins.
    3. A peaceful and comfortable personal environment for Lains when they are recreating or sleeping. Specifically, an environment that is:
        - Large enough to not feel claustraphobic, roughly 10' x 10' at least.
        - Sheltered from the outside elements, kept dry, bug-free, and at an ambient temperature.
        - Can be either lit up or darkened.
    4. A clean personal environment, including clean sheets and clothes.
    5. Running plumbing suitable for urination/defecation, showering, and other basic hygine tasks.
    6. Regular physical activity, something like being able to walk a mile every day.
    
    Psychological Needs
    
    1. A sense of community/companionship.
    2. A sense of competence/self-actualization, the ability/opportunity to solve difficult problems.
    3. A cultural connection to the outside world, via internet and media.
    
Also, if anyone has any suggestions/criticism for the #commune channel or other associated services relating to this project, please let me know here, the thread, IRC or by email.


## nyanstar
-What do you hope to accomplish by living in a laincommune? (please try to answer in 'I' statements as much as possible)
Live an easier, more pleasant life by reducing the amount of needed wageslavery, and increasing the amount of meaningful work.
Be around intersting people.
Get as free from society as I can (living as a hermit alone is hard, with a small group it becomes easier), while maintening enough of a connexion to provide me with books and internet access.

-What values would you like to see the community embody/espouse? 
(please try to be concise, <10)
Individual freedom.
Self reliance of the commune, maybe not off the grid but ready to if necessary.
Self-improvment.
Art as the highest human pursuit.
Sobriété heureuse (Happy sobriety ?)

-What type of environment do you envision the commune to be located in? (urban/rural, continent, climate etc)
Europe, warm climate, rural but not too far away from civilization.

-What can you contribute, in terms of capital or labor, to starting up the commune?
Right now, an able body. In a few months, a reasonable monetary income, maybe 1000€/month after covering basic needs. In a few years, a few tens of thousand euros and/or a house and a land.
Also a few basic wood working tools and a sewing machine if that's any useful.

-Once the commune is active, what can you contribute to the community on an ongoing basis?
Earn money as code monkey.
Teach basic CS to other members.
Whatever an able body can do, farming, construction, cooking ...
Beside computers, I have meaningful experience with : cooking, repairing clothes.
Meaningless experience with : making clothes from fabric, woodworking.

-Do you have any restrictions that would effect the location or necessary commune services, disabilities, serious illness, allergies, drug addiction, etc?
Nay.

-Do you have experience living in a commune? starting a commune?
Nay.

## N0
         -What do you hope to accomplish by living in a laincommune?
         Being a musician doesn't pay very well, I would like to be able to not die without selling my sould to some megacorp for money.
         -What values would you like to see the community embody/espouse?
         anti-wage slavery, pro artistic and individual freedom, anarchist in organisation. 
         -What type of environment do you envision the commune to be located in?
         I don't care I live in the Wired my meatspace location makes little difference. I don't particularly like the heat but I'll survive.
         -What can you contribute, in terms of capital or labor, to starting up the commune?
         I'm a poorfag, but i can do hard labor I'm pretty strong. I can cook okay, I'm pretty much happy to do whatever is need of me 
         -Once the commune is active, what can you contribute to the community on an ongoing basis?
         I can busk for money, music and other things. I have skills surviving in an urban environment (finding food etc)
         -Do you have any restrictions that would effect the location or necessary commune services, disabilities, serious illness, allergies, drug addiction, etc?
         not really.
         -Do you have experience living in a commune? starting a commune?
         nope.

##bt

    -What do you hope to accomplish by living in a laincommune?

I enjoy studying, writing, and gardening I'd like to be able to do these things full time, and while I have plans to do this on my own otherwise, I'd enjoy the companionship of a collective.

    -What values would you like to see the community embody/espouse?

bookishness, creativity, kindness, self-reliance (of the community not individuals), & respect (for one another, and the community)

    -What type of environment do you envision the commune to be located in? (urban/rural, continent, climate etc)

well, I like gardening so rural would be better for me, I also like a change of seasons and rain, so not the subtropics, the tropics, or the desert. I live in the US so north America would be nice, I made [a page](http://commun
e.muskox.me:4567/Places/United-States) with some further comments. All in all I'd like for the commune to be closest to the maximum number of lains and somewhere with good growing conditions. I'd be gladly move to somewhere a b
it dryer and hotter if that's what it took.

    -What can you contribute, in terms of capital or labor, to starting up the commune?

labor certainly, research as well as can be seen by my earlier mentioned details on US land and my other work with B. F. Skinner and Twin Oaks. I'm looking into means of generating capital currently, atm I'm a starving college
student.

    -Once the commune is active, what can you contribute to the community on an ongoing basis?

I have minimal computer science and political training from college, but I imagine that I'll be working in agriculture or where ever else I'm needed. (this is my preference as well)

    -Do you have any restrictions that would effect the location or necessary commune services, disabilities, serious illness, allergies, drug addiction, etc?

I have very fortunate genetics, and I don't even drink.

    -Do you have experience living in a commune? starting a commune?

no, and no; however, sometime before winter I'll be moving to ~5acres with my folks and I'll be feeding myself completely and my folks supplementary after the spring of next year. At some point I'll also be setting up a small o
ff grid home on the land provided nothing happens.   
