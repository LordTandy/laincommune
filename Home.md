# Lain Commune's Wiki

The git repository that contains this wiki's data is replicated at
[https://gitlab.com/nyanstar/laincommune](https://gitlab.com/nyanstar/laincommune)

The gitlab repo should be updated automatically after every edit.

## Threads

[#1](https://lainchan.org/%CE%94/res/788.html)
[archive](https://web.archive.org/web/20181022060144/https://lainchan.org/%CE%94/res/788.html)

[#2](https://lainchan.org/%CE%94/res/1784.html)

## IRC

irc.lainchan.org:+6697 #commune

Every Sunday at 20:00 UTC

## Initial OP

> For the past five or ten years, I've been seeing a consistent theme across imageboard and irc culture: A depressed, lonely 20-something who, while is smart / has interests (especially in academics or engineering), doesn't fit into the typical role of a "successful person". They have (often anxiety-related) motivation problems leading to failure in school, social handicaps which prevent them from networking as well as they would need to to break into an industry without a degree, and high aspirations which won't allow them to feel fulfilled with working behind a cashier or at a warehouse for the rest of their lives. (You can see a lot of this over at >>>/hum/3469)

> The ideal situation to be in, then, would be for them to be a NEET: supported by the government or family, they fellowship with their friends, work on projects they find interesting, and have the time/space to engage in self-care that they wouldn't otherwise be able to do. This kind of person who achieves NEETdom is happier, healthier, and more well-rounded than their wageslave counterpart.

> The issue is: how does one obtain NEETdom? If you're not born into a wealthy, tolerant family, or are a natural citizen of a very liberal government, you're pretty much fuarrrked.

> What if, instead of relying on other people, we achieved NEETdom through our own means? The requirements a person needs to live a good life are relatively simple: Shelter, running water/plumbing, heating and cooling, food, power, internet, and probably a few more small things I'm forgetting. What's stopping us from buying a plot of land and building our own infrastructure, our own buildings and automated farms and power sources, and living by our own means?

> - - -

> To facilitate discussion in this thread beyond whether a lain commune would be feasible or desirable (which is discussion I hope we have as well), here are some basic questions:
> How do we get initial capital? Obviously, trying to squeeze money out of a bunch of NEETs is like trying to bleed a rock.

> How do we sustain ourselves once we've been established? The less resources we import the better, but we'll still have to pay taxes on land at the very least.

> Where to build? In a first-world place like North America or Europe? Something more rural like South America, Africa, South Asia, ...?

> How do we govern ourselves? If a democratic system, how the infrastructure set up / how is it resistant to attack? If a dictatorship or oligarchic situation, how to ensure that the people don't end up getting shafted?

> How do we ensure that the community has a good culture? I quite like that lainchan culture as a whole right now, but obviously we're not without our flaws. A community of people who aren't good at socializing might be particularly vulnerable to certain failure states.