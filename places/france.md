# France

## Real estate taxes

(whoever knows the proper english name can fix this)

There are two main taxes related to real estate : one for the owner of the property, and one for the person who lives on it. They are both fixed and collected by the local government (town level).

* "Taxe d'habitation" :
for the person who lives there :
The amount of the tax is a percentage of the amount of the rent. If you're the owner and don't actually pay a rent, take a wild guess as to how much you would rent the place for. The percentage depends on the town, on average it's ??? 15% ??? (varies wildly, and there are various funny administrative caveats that may lower or increase the final amount).

* "Taxe foncière":
for the owner :
Idem as the previous one I think. The base percentage is 50%, which is further multiplied by a percentage decided by the local government. On average it's ??? 20% ??? (varies wildly as well, from 10% to 90%). One can be exempted of this tax under various conditions such as being old and poor or repaying the loan that payed for the property.


## Real estate prices in the south-east countryside

(Some examples from a local agency)

NB 1 acre ~ 4 000m²

### Bare land

* 33 000€ for ~800m²
* 35 000€ for ~560m²
* 44 000€ for ~860m²
* 59 000€ for ~1 300m²
* 59 000€ for ~1 000m²
* 69 000€ for ~1 800m²
* 75 000€ for ~700m²
* 85 000€ for ~2 000m²

all with water, power and sewage grid on the border ; and within biking distance of the nearest village with groceries shop

They are also lands where you're allowed to build a house ("terrain constructible"). Land where building is forbidden ("terrain non constructible") is on average 20 (twenty) times cheaper. But even non-permanent dwellings like yurts are forbidden. Upgrading a land from one status to the other is not impossible, but obviously difficult or the prices wouldn't be so different. You basically have to convince the town administration that it would be beneficial to the town for you to build here.

### House

* 59 000€ for 2 bedrooms, 120m² house, needs renovations
* 115 000€ for 3 bedrooms, 85m² house on 620m² land, needs renovations
* 120 000€ for 4 bedrooms, 170m² house, needs renovations
* 129 000€ for 3 bedrooms, 135m² house, needs cleanup
* 140 000€ for 2 bedrooms, 90m² house on 530m² land
* 152 000€ for 3 bedrooms, 105m² house on 830m² land, needs cleanup
* 229 000€ for 5 bedrooms, 150m² house
* 242 000€ for 4 bedrooms, 135m² house on 2300m² land 

etc etc

### Other
* 30 000€ for 140m² shop with toilets (legal to live in ? probably, but need to build everything, bedrooms, kitchen etc...)
* 58 000€ for 9 rooms 200m² office with toilets, needs renovation
* 77 000€ for 5 rooms 195m² office with toilets, needs cleanup
* 89 000€ for building with 1 shop and 3 1-room apartments
* 275 000€ for building with 1 2-bedroom apartment and 2 1-bedroom apartment, 200m² on 1 000m² land