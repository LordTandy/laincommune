# How to get the essential resources for day to day life

## Money

* remote freelancing: webdev, programming, and writing
* Doing tech work for other communes/squats/neigbours in the vicinity
* farming precious crops: mushrooms, aromatic herbs ... (can be done indoors)
* fixing computers for normies (yeah it's boring but it's pretty steady if we just need cash)
* busking/street performing (lots of squatters do this) (this will be difficult for lains with autism)
* investing/swing trading/day trading/
* cryptocurrency mining (not worth it unless we steal someone's power, which could get us in trouble)
* Starting a hackerspace and letting outsiders use our tools and some of our facilities
* hosting
* scraping/salvage ('''e-waste recycling''' etc)
* farming? (even professionals with lots of mechanization seem to have a hard time making a living from it these days)
* Solar/Hydroelectric/wind energy generation and grid resale (return on investment takes near a decade I think, at least for solar + location dependant)
* -https://www.turbulent.be/
* Flipping cars
* -http://flippingincome.com/flipping-cars-for-profit/
* Pet boarding
* bee keeping

## Shelter

* Apartment (this is a good choice starting out because there are no commitments and we might end up not liking the commune)
* House a Lain already owns (is there any ?) I know a place in east texas with a government lean on it in a small college town that has been unoccupied for multiple years.
* squat
* Seastead (we would all need to be comfortable being isolated even more so than a rural settlement)(and  the monetary cost would be HUGE)
* Island (which islands do you guys have in mind?)
* Homestead
* Vans/RVs/'Tents'/Carcamping No reason we cannot all car camp, at least temporarily or set up hexayurts in a rural location
* http://hexayurt.com/
* https://www.cheaprvliving.com/boondocking-2/prison-break-part-ii-how-to-live-in-a-car/ 
* property guardian

## Energy

* wood for heating and cooking

## Food

* easy, filling, nutritious crops : potatoes, cabbages, carrots, beans
* dumpster diving