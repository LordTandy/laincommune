# Maine

-Low property taxes

-Easy to attain neetbux

-Plenty of undeveloped land

-Large swaths of land and townships/towns have no building codes, none. This means people can dig a hole and then sleep in it without the cops arresting them for it, and it means computer nerds and hippies can try all kinds of types of housing they want. 

-Land is fertile unless you dig into granite, don't do that obviously.

-Water table is good many many Mainers dig their own wells.

-Land has many animals to hunt. (This means more food.)

-Protection of environment is a part of Maine culture. 

-A sort of individualist "live and let live" thing is a part of the culture too. Summarized as "mind your bisnus." 

-If your're kind and helpful to your neighbors when they need and leave them alone when they don't then they will do the same for you.

-There is very very little crime in Maine, and almost everyone has guns. In the rare chance something bad happens people defend themselves.

-not much in the way of public transportation outside of South Portland / Twin Cities / regular Portland / Westbrook (although the surrounding cities in the twin cities area do connect to their public transportation system)
